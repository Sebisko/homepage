
let header = document.querySelector('#header');
let hamMenu = document.querySelector('#ham-menu');
let headerMenu = document.querySelector('#header-menu');
let body = document.querySelector('body');

function menuToggle() {

    header.classList.toggle('header-active');
    hamMenu.classList.toggle('ham-menu-active');
    headerMenu.classList.toggle('menu-active');

    if (header.classList.contains("header-active")) {

        body.style.overflow = 'hidden';

    } else {

        body.style.overflow = 'scroll';
    }

}

function menuAnchor() {

    header.classList.remove('header-active');
    hamMenu.classList.remove('ham-menu-active');
    headerMenu.classList.remove('menu-active');

    if (header.classList.contains("header-active")) {

        body.style.overflow = 'hidden';

    } else {

        body.style.overflow = 'scroll';
    }

}



